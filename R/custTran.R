# Transform input data to the format required be the model for prediction
cTransform <- function(inputDF) {
  slog("INFO", "This is customized transformation for input data - ")
  slog("INFO", "This is how to call logger with INFO - ")
  slog("WARN", paste("This is how to ", "call logger with WARN - ", sep = ''))
  slog("ERROR", "This is how to call logger with ERROR - ")
  slog("DEBUG", paste("This is how to ", "call logger with DEBUG - ", sep = ''))
  createOrReplaceTempView(inputDF, 'inputDF')
  inputDF <- sql("SELECT *, IF(quality > 6, 'good'  ,IF(quality < 6, 'bad','average' )) AS taste FROM inputDF")
  #inputDF$taste <- ifelse(inputDF$quality == 6,'normal', ifelse(inputDF$quality < 6, 'bad', 'good'))
  inject_underscore <-
    function(df) {
      names(df) <- gsub(" ", "_", names(df))
      df
    }
  inputDF <- inject_underscore(inputDF)
  inputDF$fixed_acidity <-
    cast(inputDF$fixed_acidity, "float")
  inputDF$volatile_acidity <-
    cast (inputDF$volatile_acidity, "float")
  inputDF$citric_acid <-
    cast (inputDF$citric_acid, "float")
  inputDF$residual_sugar <-
    cast (inputDF$fixed_acidity, "float")
  inputDF$chlorides <-
    cast (inputDF$residual_sugar, "float")
  inputDF$free_sulfur_dioxide <-
    cast (inputDF$free_sulfur_dioxide, "float")
  inputDF$total_sulfur_dioxide <-
    cast (inputDF$total_sulfur_dioxide, "float")
  inputDF$density <- cast (inputDF$density, "float")
  inputDF$pH <- cast (inputDF$pH, "float")
  inputDF$sulphates <- cast (inputDF$sulphates, "float")
  inputDF$alcohol <- cast (inputDF$alcohol, "float")
  #inputDF$taste <- cast (inputDF$taste, "string")
  #input_tranDF <- drop(inputDF, "quality")
  return(inputDF)
}

# Tranform the prediction results to the format for writing the the destination
dTransform <- function(outputDF) {
  slog("INFO",
       "This is customized transformation for predictive results - ")

  outputDF <- select(outputDF, "fixed_acidity", "volatile_acidity", "citric_acid", "residual_sugar", "chlorides", "free_sulfur_dioxide", "total_sulfur_dioxide", "density", "pH", "sulphates", "alcohol", "taste" , "prediction" )
  output_tranDF <- outputDF

  return(output_tranDF)
}
