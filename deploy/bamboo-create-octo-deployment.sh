#!/bin/bash
set -e

#environment="Environments-1"
environmentid=$1
echo "environmentid: $environmentid"

octopuskey=$2
#echo "octopuskey: $octopuskey"

octohost=$3
echo "octohost: $octohost"

releaseid=$(jq '.Id' < createreleaseresponse.json)
echo "ReleaseId: $releaseid"

echo "{\"ReleaseID\":${releaseid}, \"EnvironmentID\":\"${environmentid}\"}}" > createdeploymentrequest.json
curl -X POST ${octohost}/api/deployments -H "X-Octopus-ApiKey: ${octopuskey}" -H "Accept: application/json" -d @createdeploymentrequest.json
