#!/bin/bash
set -e

packageversion=$1
echo "PackageVersion: $packageversion"

#projectid="Projects-161"
projectid=$2
echo "octoProjectId: $projectid"

octopuskey=$3
#echo "octopuskey: $octopuskey"

octohost=$4
echo "octohost: $octohost"

echo "Create New Release"
echo "{\"ProjectId\":\"${projectid}\", \"Version\": \"${packageversion}\", \"SelectedPackages\":[{\"StepName\":\"DeployModelToHdfs\",\"Version\":\"${packageversion}\"}, {\"StepName\":\"SubmitHDInsightCustomAction\",\"Version\":\"${packageversion}\"}]}" > createreleaserequest.json
curl -X POST ${octohost}/api/releases -H "X-Octopus-ApiKey: ${octopuskey}" -H "Accept: application/json" -d @createreleaserequest.json > createreleaseresponse.json
