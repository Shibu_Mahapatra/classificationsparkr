#!/bin/bash
set -e

packageversion=$1
echo "packageversion: $packageversion"

octopuskey=$2
#echo "OctopusKey: $octopuskey"

octohost=$3
echo "OctoHost: $octohost"


curl -X POST ${octohost}/api/packages/raw?replace=true -H "X-Octopus-ApiKey: ${octopuskey}" -F "data=@kmeansmodel.${packageversion}.zip"
