#!/bin/bash

basedir="/application/sentience/kmeansmodel"

tr -d '\n' < ../score/conf/model.properties > ../score/conf/model.properties.temp
rm ../score/conf/model.properties
mv ../score/conf/model.properties.temp ../score/conf/model.properties

hadoop fs -mkdir -p ${basedir}
hadoop fs -copyFromLocal -f ../model ${basedir}
hadoop fs -copyFromLocal -f ../conf ${basedir}
hadoop fs -copyFromLocal -f ../job ${basedir}
hadoop fs -copyFromLocal -f ../data ${basedir}
hadoop fs -copyFromLocal -f ../results ${basedir}
