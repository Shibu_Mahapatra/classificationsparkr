#!/bin/bash

#this script runs as HDInsight Custom Action Script
set -e
echo "param recived: ${1}"
param_array=(`echo ${1} | sed -e 's/,/\n/g'`)

blobfoldername=${param_array[0]}
pkgtarfilename=${param_array[1]}
storageaccountname=${param_array[2]}
storageaccountcontainername=${param_array[3]}

echo "PackageTarFileName: ${pkgtarfilename}"
echo "StorageAccountName: ${storageaccountname}"
echo "StorageAccountContainerName: ${storageaccountcontainername}"

#create temp directory to hold the R package tar file
mkdir -p /tmp
rm -f /tmp/${pkgtarfilename}
#donwload R package tar file from Azure Storage
echo "download R package tar file from Azure storage"
hadoop fs -copyToLocal wasb://${storageaccountcontainername}@${storageaccountname}.blob.core.windows.net/${blobfoldername}/${pkgtarfilename} /tmp/

#install R pacakge
echo "Installing R pacakge"
R CMD INSTALL /tmp/${pkgtarfilename}

#cleanup
rm -f /tmp/${pkgtarfilename}
