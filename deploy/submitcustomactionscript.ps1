<#
This scripts runs on Octo Deploy server, it executes the following steps:
1. Uploads kmeansmodel pkg tar file to Spark Cluster Default Storage
2. Uploads custom action script bash file to Spark Cluster Default Storage
3. Submit the HDInsight Custom Action Script to Spark Cluster as specified below.
Note To Do: make nameing generic so changes won't be needed per model.

kmeansmodelpkgtarfile parameter value is expected to be relative path to kmeansmodelpkgtarfile
#>
param($clusterName, $storageResourceGroupName, $storageAccountName, $storageAccountContainerName, $pkgversion)
$blobFolderName = "kmeansmodel"
$kmeansmodelpkgtarfilename = "kmeansmodel_${pkgversion}.tar.gz"

Write-Output "Upload scripts to Spark Cluster default storage and then submit HDInsight Custom Action Script"

write-output "PsScriptRoot - $PSScriptRoot"
write-output "StorageResourceGroupname: $storageResourceGroupName"
Write-Output "StorageAccountName: $storageAccountName"
Write-Output "StorageAccountContainerName: $storageAccountContainerName"
write-Output "pkgversion: $pkgversion"
Write-Output "kmeansmodelpkgtarfile - $kmeansmodelpkgtarfilename"

$installkmeansmodelpkgFile = ".\installkmeansmodelpkg.sh"

Write-output "installkmeansmodelpkgfile: $installkmeansmodelpkgFile"

# Get the storage account key
$storageAccountKey = (Get-AzureRmStorageAccountKey -ResourceGroupName $storageResourceGroupName -Name $storageAccountName)[0].Value

# Create the storage context object (authenticates to the Azure Storage REST API)
$destContext = New-AzureStorageContext -StorageAccountName $storageAccountName -StorageAccountKey $storageaccountkey

#Get Container Object
$containerObj = Get-AzureStorageContainer -Context $destContext | Where-Object { $_.Name -eq $storageAccountContainerName }

if ($containerObj -eq $null)
{
#Container does not exist so create it
   New-AzureStorageContainer -Name $storageAccountContainerName -Context $destContext
}

# Upload the file from local computer's filesystem to the Blob Container
$filePath = "$($PSScriptRoot)\${kmeansmodelpkgtarfilename}"
$blobName = Split-Path -Path $filePath -Leaf
$blobName = $blobFolderName + "/" + $blobName
Set-AzureStorageBlobContent -File $filePath -Container $storageAccountContainerName -Blob $blobName -Context $destContext -Force


$filePath = "$($PSScriptRoot)\installkmeansmodelpkg.sh"
$blobName = Split-Path -Path $filePath -Leaf
$blobName = $blobFolderName + "/" + $blobName
Set-AzureStorageBlobContent -File $filePath -Container $storageAccountContainerName -Blob $blobName -Context $destContext -Force

#Submit Custom Action to HDInsight

$scriptActionUri = "https://$($storageAccountName).blob.core.windows.net/$($storageAccountContainerName)/$($blobName)"
Write-Output "InstallScriptUri: $($scriptActionUri)"
$scriptActionName = "kmeansmodel"
# The node types that the script action is applied to
$nodeTypes = "headnode", "workernode"


 $existingScriptAction = get-AzureRmHDInsightPersistedScriptAction -ClusterName $clusterName -Name $scriptActionName

#there cannot be multiple Persisted Script Actions at a time so remove if it exists
 if ($existingScriptAction -ne $null)
 {
     Remove-AzureRmHDInsightPersistedScriptAction -ClusterName $clusterName -Name $scriptActionName
 }

# Apply the script and mark as persistent
$result = Submit-AzureRmHDInsightScriptAction -ClusterName $clusterName `
    -Name $scriptActionName `
    -Uri $scriptActionUri `
    -NodeTypes $nodeTypes `
    -PersistOnSuccess `
    -Parameters "$($blobFolderName),$($kmeansmodelpkgtarfilename),$($storageAccountName),$($storageAccountContainerName)"

if ($result.OperationState -eq "Succeeded")
{
    exit 0
}
else
{
    exit 1
}
