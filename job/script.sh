#!/bin/sh
export SPARK_HOME=/usr/hdp/current/spark2-client
$SPARK_HOME/bin/spark-submit --master yarn --deploy-mode cluster --driver-memory 1024m --num-executors 2 --executor-memory 1024m --executor-cores 2 $1 $2
